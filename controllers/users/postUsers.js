const bcrypt = require("bcrypt");
const { insertUser, selectUserById } = require("../../db/queries/users");
const { newUserSchema } = require("../../schemas/users");
const { processAndSaveImg} = require("../../helpers");

const postUsers = async (req, res, next) => {
  try {
    await newUserSchema.validateAsync(req.body);

    const avatar = req.files?.avatar;

    let avatarName;

    if (avatar) {
      avatarName = await processAndSaveImg(avatar.data);
    }

    const { password } = req.body;

    const hashedPassword = await bcrypt.hash(password, 10);

    const userId = await insertUser({
      ...req.body,
      password: hashedPassword,
      avatar: avatarName,
    });

    const createdUser = await selectUserById(userId);

    delete createdUser.password;

    

    res.status(201).send({ status: "ok", data: createdUser });
  } catch (error) {
    next(error);
  }
};

module.exports = postUsers;
