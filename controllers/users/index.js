const loginUser = require("./loginUser");
const getProfile = require("./getProfile");
const deleteUser = require("./deleteUser");
const postUsers = require("./postUsers");
const editUser = require("./updateUser");

module.exports = { postUsers, loginUser, getProfile, deleteUser, editUser };
