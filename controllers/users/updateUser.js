const { updateUser } = require("../../db/queries/users");
const { generateError  } = require("../../helpers");
const bcrypt = require("bcrypt");


const editUser = async (req, res, next) => {
  try {
    const { name, mail, password, nick} = req.body; 
    const {user_id} = req.auth;
    
    if (!name || !mail|| !password || !nick || !user_id) {
      generateError("Missing required fields", 400);
    }
    
    const hashedPassword = await bcrypt.hash(password, 10);
    
    const updatedId = await updateUser({
    name,
    mail, 
    password: hashedPassword, 
    nick,
    user_id,
    });

    if (!updatedId) {
      generateError("User not found or unauthorized to update", 404);
    }

    res.status(200).send({
      status: "ok",
      message: "User updated successfully",
      updatedId,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = editUser;