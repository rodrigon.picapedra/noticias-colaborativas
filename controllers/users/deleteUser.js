const { deleteUserById, selectUserById } = require("../../db/queries/users");
const { generateError } = require("../../helpers");

const deleteUser = async (req, res, next) => {
  try {
    const { id } = req.params;
    
    const user = await selectUserById(id);

    if (!user) {
      generateError("User doesn't exist", 404);
    }

    await deleteUserById(id);

    res.status(200).send({ status: "ok", message: "User deleted succesfully" });
  } catch (error) {
    next(error);
  }
};

module.exports = deleteUser;
