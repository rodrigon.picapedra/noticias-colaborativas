const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { selectUserByEmail } = require("../../db/queries/users");
const { generateError } = require("../../helpers");
const { loginUserSchema } = require("../../schemas/users");

const loginUser = async (req, res, next) => {
  try {
    await loginUserSchema.validateAsync(req.body);

    const { mail, password } = req.body;

    const userDB = await selectUserByEmail(mail);

    if (!userDB) {
      generateError("No existe el usuario", 400);
    }

    const isPasswordOk = await bcrypt.compare(password, userDB.contrasena);

    if (!isPasswordOk) {
      generateError("El email o la contraseña son incorrectos", 400);
    }

    const { user_id, nombre } = userDB;

    const tokenData = { user_id, mail, nombre };

    const expiresIn = "30d";

    const token = jwt.sign(tokenData, process.env.JWT_SECRET, {
      expiresIn,
    });

    res.send({ status: "ok", data: { token, expiresIn } });
  } catch (error) {
    next(error);
  }
};

module.exports = loginUser;
