const { selectUserById } = require("../../db/queries/users");

const getProfile = async (req, res) => {
  const user = await selectUserById(req.auth.user_id);
  delete user.contrasena;

  res.send({ status: "ok", data: user });
};

module.exports = getProfile;
