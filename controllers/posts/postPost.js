const { insertPost, selectPostById } = require("../../db/queries/posts");
const { newPostSchema } = require("../../schemas/posts");
const { processAndSaveImg} = require("../../helpers");

const postPost = async (req, res, next) => {
  try {
    await newPostSchema.validateAsync(req.body);
    
    const { title, entry, text, topic} = req.body;
    const image = req.files?.image;
    let imageName;
    if (image) {
       imageName = await processAndSaveImg(image.data);
    }
    

    
    
    const postId = await insertPost({
      user_id: req.auth.user_id,
      title,
      entry,
      text,
      topic,
      image: imageName,
    });

    const post = await selectPostById(postId);

    res.status(201).json({
      status: "ok",
      message: "Post created successfully",
      data: post,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = postPost;
