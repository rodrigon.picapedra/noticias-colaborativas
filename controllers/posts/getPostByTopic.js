const { selectPostByTopic } = require("../../db/queries/posts");
const { generateError } = require("../../helpers");

const getPostsByTopic = async (req, res) => {
  const { topic } = req.params;
  
  const posts = await selectPostByTopic(topic);
  
  // if (!posts.length) {
  //   generateError("Post doesn't exist", 404);
  //  }

  res.send({ status: "ok", data: posts });
};

module.exports = getPostsByTopic;

