const { deletePostById, selectPostById } = require("../../db/queries/posts");
const { generateError } = require("../../helpers");

const deletePost = async (req, res, next) => {
  try {
    const { id } = req.params;

    const post = await selectPostById(id);

    if (!post) {
      generateError("Post doesn't exist", 404);
    }

    await deletePostById(id);

    res.status(200).send({ status: "ok", message: "Post deleted succesfully" });
  } catch (error) {
    next(error);
  }
};

module.exports = deletePost;
