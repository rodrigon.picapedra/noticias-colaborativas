const { updatePost } = require("../../db/queries/posts");
const { generateError, processAndSaveImg,  } = require("../../helpers");


const editPost = async (req, res, next) => {
  try {
    const { title, entry, text, topic} = req.body; 
    const { id : post_id } = req.params;
    const {user_id} = req.auth;
    
    if (!title || !entry || !text || !topic || !user_id || !post_id) {
      generateError("Missing required fields", 400);
    }
    const image = req.files?.image;
    let imageName;
    if (image) {
       imageName = await processAndSaveImg(image.data);
    }
    
    
    
    const updatedId = await updatePost({
      title,
      entry,
      text,
      topic,
      image: imageName,
      user_id,
      post_id,
    });

    if (!updatedId) {
      generateError("Post not found or unauthorized to update", 404);
    }

    res.status(200).send({
      status: "ok",
      message: "Post updated successfully",
      updatedId,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = editPost;