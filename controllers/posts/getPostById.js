const { selectPostById } = require("../../db/queries/posts");
const { generateError } = require("../../helpers");

const getPostsById = async (req, res) => {
  const { id } = req.params;
  const posts = await selectPostById(id);
  
  // if (!posts.length) {
  //   generateError("Post doesn't exist", 404);
  // }

  res.send({ status: "ok", data: posts });
};

module.exports = getPostsById;

