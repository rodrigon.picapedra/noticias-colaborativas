const { selectPosts } = require("../../db/queries/posts");

const getPosts = async (req, res) => {
  const posts = await selectPosts();
  
  res.send({ status: "ok", data: posts });
};

module.exports = getPosts;

