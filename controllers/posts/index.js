const getPosts = require("./getPosts");
const postPost = require("./postPost");
const deletePost = require("./deletePosts");
const editPost = require("./updatePost");
const getPostsById = require("./getPostById");
const getPostsByTopic = require("./getPostByTopic");
module.exports = { getPosts, postPost, deletePost, editPost, getPostsById, getPostsByTopic };
