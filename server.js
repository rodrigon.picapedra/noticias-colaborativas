require("dotenv").config();
const path= require("path");
const express = require("express");
const fileUpload = require("express-fileupload");
const cors = require("cors");

const app = express();

const { getPosts,
        postPost,
        deletePost,
        editPost,
        getPostsById,
        getPostsByTopic,
} = require("./controllers/posts");
const {
  postUsers,
  loginUser,
  getProfile,
  deleteUser,
  editUser,
} = require("./controllers/users");
const {
  validateAuth,
  handleError,
  handleNotFound,
} = require("./middlewares");


app.use("/image", express.static(path.join(__dirname,"uploads")));
app.use(express.json());
app.use(fileUpload());
app.use(cors());

app.get("/posts", getPosts);
app.post("/users/register", postUsers);
app.post("/users/login", loginUser);
app.delete("/users/delete/:id", validateAuth, deleteUser);
app.delete("/post/delete/:id", validateAuth, deletePost);
app.put("/post/edit/:id", validateAuth, editPost );
app.get("/posts/:id", getPostsById );
app.get("/posts/filter/:topic", getPostsByTopic);
app.put("/user/edit", validateAuth, editUser);


app.get("/profile", validateAuth, getProfile);
app.post("/posts/new_post", validateAuth, postPost);

app.use(handleError);

app.use(handleNotFound);

app.listen(5500, () => {
  console.log("Server running on http://localhost:5500");
});

