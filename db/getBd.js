const mysql = require("mysql2/promise");

const { HOST, PORT, USER, PASSWORD, DATABASE } = process.env;

let pool;

const getPool = async () => {
  if (!pool) {
    pool = mysql.createPool({
      host: HOST,
      port: PORT,
      user: USER,
      password: PASSWORD,
      database: DATABASE,
      connectionLimit: 10,
      timezone: "local",
    });
  }

  return pool;
};

module.exports = getPool;
