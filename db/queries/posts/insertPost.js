const getPool = require("../../getBd");

const insertPost = async ({ title, entry, text, topic, image, user_id }) => {
  const pool = await getPool();

  const [{ insertId }] = await pool.query(
    `INSERT INTO noticias (titulo, entrada, texto, tema, foto,  user_id) VALUES (?, ?, ?, ?, ?, ?)`,
    [title, entry, text, topic,image, user_id]
  );
  
  return insertId;
};

module.exports = insertPost;
