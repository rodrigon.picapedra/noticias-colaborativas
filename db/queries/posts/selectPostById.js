const getPool = require("../../getBd");

const selectPostById = async (id) => {
  const pool = await getPool();

  const [post] = await pool.query("SELECT n.id_noticias, n.titulo, n.foto, n.entrada, n.texto, n.tema, n.user_id, u.nombre, u.nick, u.mail FROM Noticias n LEFT JOIN Usuario u ON n.user_id = u.user_id WHERE id_noticias = ?", [id]);
  
  return post;
};

module.exports = selectPostById;
