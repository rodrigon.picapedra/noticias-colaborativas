const getPool = require("../../getBd");

const updatePost = async ({ title, entry, text, topic, image, user_id ,post_id }) => {
  const pool = await getPool();

  const updateId = await pool.query(
    `UPDATE noticias SET titulo = ?, entrada = ?, texto = ?, tema = ?, foto = ? WHERE id_noticias = ? AND user_id = ?`,
    [title, entry, text, topic, image ,post_id, user_id, ]
  );
    
  
  return updateId;
};

module.exports = updatePost;
