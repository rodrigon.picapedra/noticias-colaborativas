const selectPosts = require("./selectPosts");
const insertPost = require("./insertPost");
const selectPostById = require("./selectPostById");
const deletePostById = require("./deletePostById");
const updatePost = require("./updatePost");
const selectPostByTopic = require("./selectPostByTopic");

module.exports = { selectPosts, insertPost, selectPostById, deletePostById, updatePost, selectPostByTopic };
