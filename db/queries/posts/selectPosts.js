const getPool = require("../../getBd");

const selectPosts = async () => {
  const pool = await getPool();

  const [posts] = await pool.query("SELECT n.id_noticias, n.titulo, n.foto, n.entrada, n.texto, n.tema, n.user_id, u.nombre, u.nick, u.mail FROM Noticias n LEFT JOIN Usuario u ON n.user_id = u.user_id ORDER BY id_noticias DESC");
  
  return posts;
};

module.exports = selectPosts;
