const getPool = require("../../getBd");

const deletePostById = async (id) => {
  const pool = await getPool();

  await pool.query("DELETE FROM noticias WHERE id_noticias = ?", [id]);
};

module.exports = deletePostById;
