const getPool = require("../../getBd");

const selectPostByTopic = async (topic) => {
  const pool = await getPool();

  const [post] = await pool.query("SELECT * FROM noticias WHERE tema = ?", [topic]);
  
  return post;
};

module.exports = selectPostByTopic;
