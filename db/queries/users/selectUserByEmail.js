const getPool = require("../../getBd");

const selectUserByEmail = async (mail) => {
  const pool = await getPool();

  const [[user]] = await pool.query("SELECT * FROM usuario WHERE mail = ?", [
    mail,
  ]);

  return user;
};

module.exports = selectUserByEmail;
