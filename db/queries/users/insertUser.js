const getPool = require("../../getBd");

const insertUser = async ({nick, password, name, mail}) => {
  const pool = await getPool();

  const [{ insertId }] = await pool.query(
    "INSERT INTO Usuario (nick, contrasena, nombre, mail) VALUES (?, ?, ?, ?)",
    [nick, password, name, mail]
  );

  return insertId;
};

module.exports = insertUser;
