const getPool = require("../../getBd");

const updateUser = async ({ mail, name, password, nick, user_id }) => {
  const pool = await getPool();

  const updateId = await pool.query(
    `UPDATE usuario SET nombre = ?, nick = ?, mail = ?, contrasena = ? WHERE user_id = ?`,
    [ name, nick, mail, password, user_id ]
  );
    
  
  return updateId;
};
module.exports = updateUser;
