const getPool = require("../../getBd");

const deleteUserById = async (id) => {
  const pool = await getPool();

  await pool.query("DELETE FROM usuario WHERE user_id = ?", [id]);
};

module.exports = deleteUserById;
