const getPool = require("../../getBd");

const selectUserById = async (id) => {
  const pool = await getPool();

  const [[user]] = await pool.query("SELECT * FROM usuario WHERE user_id = ?", [id]);

  return user;
};

module.exports = selectUserById;

