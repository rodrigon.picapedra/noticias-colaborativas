const insertUser = require("./insertUser");
const selectUserById = require("./selectUserById");
const deleteUserById = require("./deleteUserById");
const selectUserByEmail = require("./selectUserByEmail");
const updateUser = require("./upadateUser");

module.exports = {
  insertUser,
  selectUserById,
  deleteUserById,
  selectUserByEmail,
  updateUser,
};
