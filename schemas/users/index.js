const newUserSchema = require("./newUser");
const loginUserSchema = require("./loginUser");

module.exports = { 
    newUserSchema,
    loginUserSchema,
};
