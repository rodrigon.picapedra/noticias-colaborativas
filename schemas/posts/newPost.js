const Joi = require("joi");

const newPostSchema = Joi.object({
  title: Joi.string().min(4).max(100).required(),
  entry: Joi.string().min(4).max(100).required(),
  text: Joi.string().min(4).max(500).required(),
  topic: Joi.string().valid('Videojuegos', 'Terror', 'Corazón', 'Deportes', 'Actualidad', 'Politica', 'Cine', 'Estudios').required(),
  image: Joi.string(),
});

module.exports = newPostSchema;
