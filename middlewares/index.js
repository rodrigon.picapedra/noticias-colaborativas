const validateAuth = require("./validateAuth");
const handleError = require("./handleError");
const handleNotFound = require("./handleNotFound");

module.exports = { validateAuth, handleError, handleNotFound };

