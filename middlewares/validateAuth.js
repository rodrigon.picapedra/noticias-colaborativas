const jwt = require("jsonwebtoken");
const { generateError } = require("../helpers");

const validateAuth = (req, res, next) => {
  try {
    const { authorization } = req.headers;

    if (!authorization) {
      generateError("Authorization required", 403);
    }

    try {
      req.auth = jwt.verify(authorization, process.env.JWT_SECRET);
    } catch (error) {
      generateError("Invalid token", 401);
    }

    next();
  } catch (error) {
    next(error);
  }
};

module.exports = validateAuth;
