const generateError = require("./generateError");
const processAndSaveImg = require("./processAndSaveImg");
module.exports = { generateError, processAndSaveImg};

